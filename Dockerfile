# Rust as the base image
FROM rust:1.75 as builder

# 1. Create a new empty shell project
RUN USER=root cargo new --bin thread_bullet
WORKDIR /thread_bullet

# 2. Copy our manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# 3. Build only the dependencies to cache them
RUN cargo build --release
RUN rm src/*.rs

# 4. Now that the dependency is built, copy your source code
COPY ./src ./src

# 5. Build for release.
RUN rm ./target/release/deps/thread_bullet*
RUN cargo build --release

# The final base image
FROM ubuntu

# Copy from the previous build
COPY --from=builder /thread_bullet/target/release/thread_bullet /thread_bullet

# Run the binary
CMD ["/thread_bullet"]
