---
lang: fr
---

# Thread Bullet bot

Un bot très simple permettant de lister des fils disponibles sur un serveur
dans un message pour les rendre plus visible.

Le message est construit en utilisant des balises Markdown (`[Un
liens](https://le_lien.html)`) ce qui évite le problème des liens vers les fils
qui apparaissent en `#Inconnus` lorsque la personne n'a pas encore rejoint le
fil ou lorsqu'il est archivé.

## Commandes

### Ajouter / retirer des fils

Les commandes sont `/list` et `/unlist`

- `/list [description]` : Enregistre un fil pour qu'il soit listé par le bot. Les
  fils n'ayant pas de description, il est possible d'en ajouter une à ce moment.
- `/unlist` : retire le fil des fils listés par le bot

À noter que les fils sont automatiquement retirés des listages s'ils sont
supprimés.

Également, les postes de forums sont considérés comme des fils et qu'ils peuvent
être ajoutés aux listages.

### Créer un message de liste

Les messages de listages se créent en utilisant `/create_list` dans le salon où
la liste doit être créé.

Attention : les messages ont une taille limitée par Discord (4096 caractères).
Pour palier à cette limite, il est possible de créer un message par catégories.

- `/create_list [header] [target_category]` crée une liste recensant les fils
enregistrés, filtré sur `target_category` si fourni.
- `/get_list` recense les listes du serveur. Cette commande est utile pour
  vérifier que le bot gère activement la liste.

Cette liste se met à jour automatiquement lorsqu'un fil est ajouté / supprimé,
ou lors d'un changement de nom.

## Déploiement

Le déploiement recommandé passe par docker-compose :

```yaml
version: "3.7"
services:
  bot:
    image: registry.gitlab.com/celliern/thread_bullet
    environment:
      DISCORD_TOKEN: "votre_token_discord"
      DATA_PATH: "/app/data"
    volumes:
      - ./data:/app/data
    restart: always
```

```bash
docker-compose up -d
```
