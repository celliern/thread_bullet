use ::serenity::{futures::future::join_all, http::Http};
use anyhow::{anyhow, Error, Result};
use std::{
    collections::{BTreeMap, BTreeSet, HashSet},
    sync::{Arc, Mutex, MutexGuard},
};

use derivative::Derivative;
use dotenvy::dotenv;
use pickledb::{PickleDb, PickleDbDumpPolicy, SerializationMethod};
use poise::serenity_prelude as serenity;
use tokio::spawn;
use tokio_schedule::{every, Job};

#[derive(Debug, Eq, PartialEq, Clone, Derivative, serde::Serialize, serde::Deserialize)]
#[derivative(Hash)]
struct ListThreads {
    guild_id: serenity::GuildId,
    channel_id: serenity::ChannelId,
    message_id: serenity::MessageId,
    header: Option<String>,
    target_category: Option<serenity::ChannelId>,
}

impl ListThreads {
    fn key(&self) -> String {
        format!(
            "LIST:{}:{}:{}",
            self.guild_id, self.channel_id, self.message_id
        )
    }

    async fn from_message(
        message: serenity::Message,
        http: impl AsRef<Http>,
        header: Option<String>,
        target_category: Option<serenity::ChannelId>,
    ) -> Result<Self> {
        let http = http.as_ref();
        Ok(Self {
            guild_id: message
                .channel_id
                .to_channel(http)
                .await?
                .guild()
                .ok_or(anyhow!("Unable to convert into a guild Channel"))?
                .guild_id,
            channel_id: message.channel_id,
            message_id: message.id,
            header,
            target_category,
        })
    }
}

#[derive(
    Debug, Eq, PartialEq, PartialOrd, Ord, Clone, Derivative, serde::Serialize, serde::Deserialize,
)]
#[derivative(Hash)]
struct WatchedThread {
    guild_id: serenity::GuildId,
    thread_id: serenity::ChannelId,
    #[derivative(Hash = "ignore")]
    description: Option<String>,
}

impl WatchedThread {
    async fn guild<'a>(&self, http: impl AsRef<Http>) -> Result<serenity::PartialGuild> {
        self.guild_id
            .to_partial_guild(http.as_ref())
            .await
            .map_err(|_| anyhow!("Guild not found"))
    }
    async fn thread<'a>(&self, http: impl AsRef<Http>) -> Result<serenity::GuildChannel> {
        self.thread_id
            .to_channel(http.as_ref())
            .await?
            .guild()
            .ok_or(anyhow!("Thread not found"))
    }

    fn key(&self) -> String {
        format!("WATCHED:{}:{}", self.guild_id, self.thread_id)
    }

    fn key_from_channel(channel: serenity::GuildChannel) -> Result<String> {
        if !matches!(channel.kind, serenity::ChannelType::PublicThread) {
            return Err(anyhow!("This command must be used in a thread"));
        }
        Ok(format!("WATCHED:{}:{}", channel.guild_id, channel.id))
    }

    fn from_channel(channel: serenity::GuildChannel, description: Option<String>) -> Result<Self> {
        if !matches!(channel.kind, serenity::ChannelType::PublicThread) {
            return Err(anyhow!("This command must be used in a thread"));
        }
        Ok(Self {
            guild_id: channel.guild_id,
            thread_id: channel.id,
            description,
        })
    }
}

async fn prune_thread(http: &impl AsRef<Http>, data: &Data, thread: WatchedThread) -> Result<()> {
    if thread.thread(http).await.is_err() {
        data.db().rem(thread.key().as_str())?;
    };
    Ok(())
}

async fn prune_threads(http: impl AsRef<Http>, data: &Data) -> Result<()> {
    let futures: Result<Vec<_>> = data
        .thread_keys()?
        .into_iter()
        .map(|key| -> Result<_> {
            let thread = data.db().get(key.as_str()).unwrap();
            Ok(prune_thread(&http, data, thread))
        })
        .collect();

    let results = join_all(futures?).await;
    results.into_iter().collect::<Result<Vec<_>>>()?;
    Ok(())
}

fn filter_keys_by_gid(keys: Vec<String>, gid: serenity::GuildId) -> Vec<String> {
    keys.into_iter()
        .filter(|key| key.starts_with(&format!("WATCHED:{}:", gid)))
        .collect()
}

async fn display_threads(
    http: impl AsRef<Http>,
    threads: HashSet<WatchedThread>,
    header: Option<&str>,
    target_category: Option<serenity::ChannelId>,
) -> Result<String> {
    let http = http.as_ref();
    if threads.is_empty() {
        return Ok("No threads are being kept".to_string());
    }

    // get guild categories
    type ThreadSet = BTreeSet<(usize, WatchedThread)>;
    type ChannelMap = BTreeMap<(usize, serenity::ChannelId), ThreadSet>;
    type TreeMap = BTreeMap<Option<(usize, serenity::ChannelId)>, ChannelMap>;

    let mut tree: TreeMap = BTreeMap::new();
    for thread in threads {
        let thread_channel: ::serenity::model::prelude::GuildChannel = thread.thread(http).await?;
        let thread_position = thread_channel.position as usize;
        let parent_channel = http
            .get_channel(thread_channel.parent_id.unwrap())
            .await
            .map_err(|_| {
                anyhow!(
                    "Failed to get parent channel for thread {}",
                    thread_channel.id
                )
            })?
            .guild()
            .unwrap();

        let parent_id = parent_channel.id;
        let parent_position = parent_channel.position as usize;

        let category_id = parent_channel.parent_id;
        if let Some(category) = target_category {
            if category_id.is_none() || category != category_id.unwrap() {
                continue;
            }
        }
        let category_key = if let Some(id) = category_id {
            let category = http.get_channel(id).await?;
            Some((category.position().unwrap() as usize, id))
        } else {
            None
        };

        let categ_entry = tree
            .entry(category_key)
            .or_default()
            .entry((parent_position, parent_id))
            .or_default();
        categ_entry.insert((thread_position, thread));
    }
    let mut message = match (target_category, header) {
        (Some(_), Some(header)) => format!("# {}\n\n", header),
        (Some(_), None) => "".to_string(),
        (None, Some(header)) => format!("# {}\n\n", header),
        (None, None) => "# Fils du serveur:\n\n".to_string(),
    };

    for categ_id in tree.keys() {
        let categ_name = match categ_id {
            Some((_, id)) => {
                let categ = http
                    .get_channel(*id)
                    .await
                    .map_err(|_| anyhow!("Failed to get category"))?;
                categ.to_string()
            }
            None => "No category".to_string(),
        };
        let channels = tree.get(categ_id).unwrap().keys();
        message.push_str(&format!("## {}\n\n", categ_name));
        for chan_key in channels {
            let (_, channel_id) = chan_key;
            let channel = http
                .get_channel(*channel_id)
                .await
                .map_err(|_| anyhow!("Failed to get channel"))?;
            message.push_str(&format!("### {}\n", channel));
            for thread_key in tree.get(categ_id).unwrap().get(chan_key).unwrap() {
                let (_, thread) = thread_key;
                let guild = thread.guild(http).await?;
                let thread_chan = thread.thread(http).await?;
                message.push_str(&format!(
                    "- [{}](https://discord.com/channels/{}/{}){}\n",
                    thread_chan.name,
                    guild.id,
                    thread_chan.id,
                    thread
                        .description
                        .clone()
                        .map(|c| format!(" - {}", c))
                        .unwrap_or("".to_string())
                ));
            }
        }
    }
    Ok(message)
}

async fn update_lists(data: &Data, http: impl AsRef<Http>) -> Result<()> {
    let http = http.as_ref();
    prune_threads(http, data).await?;

    let lists = data.lists()?;
    for list in lists {
        let message_res = http.get_message(list.channel_id, list.message_id).await;
        if message_res.is_err() {
            data.remove_list(list)?;
            continue;
        }
        let mut message = message_res.unwrap();
        let threads: HashSet<WatchedThread> = data.threads_by_gid(list.guild_id)?;
        let message_content =
            display_threads(http, threads, list.header.as_deref(), list.target_category).await?;
        let embed = serenity::CreateEmbed::default().description(message_content);
        let builder = serenity::EditMessage::new().embed(embed);
        message.edit(http, builder).await?;
    }
    Ok(())
}

#[derive(Clone)]
struct Data {
    db: Arc<Mutex<PickleDb>>,
}

impl Data {
    fn new(db: PickleDb) -> Result<Self> {
        Ok(Self {
            db: Arc::new(Mutex::new(db)),
        })
    }

    fn db(&self) -> MutexGuard<PickleDb> {
        match self.db.lock() {
            Ok(guard) => guard,
            Err(poisoned) => poisoned.into_inner(),
        }
    }

    fn keys(&self) -> Result<Vec<String>> {
        Ok(self.db().get_all())
    }

    fn list_keys(&self) -> Result<Vec<String>> {
        let keys = self.keys()?;
        Ok(keys
            .into_iter()
            .filter(|key| key.starts_with("LIST:"))
            .collect())
    }

    fn thread_keys(&self) -> Result<Vec<String>> {
        let keys = self.keys()?;
        Ok(keys
            .into_iter()
            .filter(|key| key.starts_with("WATCHED:"))
            .collect())
    }

    fn tid_by_gid(&self, gid: serenity::GuildId) -> Result<Vec<String>> {
        let keys = self.thread_keys()?;
        Ok(filter_keys_by_gid(keys, gid))
    }

    fn threads_by_keys(&self, keys: Vec<String>) -> Result<HashSet<WatchedThread>> {
        let mut threads: HashSet<WatchedThread> = HashSet::new();
        let db = self.db();
        for key in keys {
            let thread = db
                .get(key.as_str())
                .ok_or(anyhow!(format!("Unable to find key {}", key)))?;
            threads.insert(thread);
        }
        Ok(threads)
    }

    fn threads_by_gid(&self, gid: serenity::GuildId) -> Result<HashSet<WatchedThread>> {
        let keys = self.tid_by_gid(gid)?;
        self.threads_by_keys(keys)
    }

    fn lists(&self) -> Result<HashSet<ListThreads>> {
        let keys = self.list_keys()?;
        let mut lists: HashSet<ListThreads> = HashSet::new();
        let db = self.db();
        for key in keys {
            let list = db
                .get(key.as_str())
                .ok_or(anyhow!(format!("Unable to find key {}", key)))?;
            lists.insert(list);
        }
        Ok(lists)
    }

    fn save_thread(&self, thread: WatchedThread) -> Result<()> {
        self.db().set(thread.key().as_str(), &thread).unwrap();
        Ok(())
    }

    fn save_list(&self, list: ListThreads) -> Result<()> {
        self.db().set(list.key().as_str(), &list).unwrap();
        Ok(())
    }

    fn remove_list(&self, list: ListThreads) -> Result<()> {
        self.db().rem(list.key().as_str()).unwrap();
        Ok(())
    }
}

type Context<'a> = poise::Context<'a, Data, Error>;

fn get_threads(ctx: Context) -> Result<HashSet<WatchedThread>> {
    ctx.data()
        .threads_by_gid(ctx.guild_id().ok_or(anyhow!("No guild id"))?)
}

/// Add this thread to the list of watched threads
#[poise::command(slash_command, prefix_command, ephemeral, guild_only)]
async fn list(
    ctx: Context<'_>,
    #[description = "Add a description that will be displayed in the lists"] description: Option<
        String,
    >,
) -> Result<(), Error> {
    let channel = ctx
        .guild_channel()
        .await
        .ok_or(anyhow!("This command must be used in a guild"))?;

    let thread = WatchedThread::from_channel(channel, description)?;
    ctx.say("Ce fil sera maintenant listé").await?;
    ctx.data().save_thread(thread.clone())?;
    update_lists(ctx.data(), ctx).await?;

    Ok(())
}

/// Remove this thread from the list of watched threads
#[poise::command(slash_command, prefix_command, ephemeral, guild_only)]
async fn unlist(ctx: Context<'_>) -> Result<(), Error> {
    let channel = ctx
        .guild_channel()
        .await
        .ok_or(anyhow!("This command must be used in a guild"))?;

    let key = WatchedThread::key_from_channel(channel)?;
    ctx.say("Ce fil ne sera plus listé").await?;
    ctx.data().db().rem(key.as_str())?;
    update_lists(ctx.data(), ctx).await?;

    Ok(())
}

/// Create a list of watched threads via the command `list`
#[poise::command(slash_command, prefix_command, ephemeral, guild_only)]
async fn create_list(
    ctx: Context<'_>,
    #[description = "Title for this list."] header: Option<String>,
    #[channel_types("Category")]
    #[description = "The category to list threads from. If not provided, all threads will be listed."]
    target_category: Option<serenity::ChannelId>,
) -> Result<(), Error> {
    ctx.say("Création de la liste...").await?;
    let threads = get_threads(ctx)?;
    let message = display_threads(ctx, threads.clone(), header.as_deref(), target_category).await?;

    // ctx.data().save_list(list)
    let embed = serenity::CreateEmbed::default().description(message);
    let builder = serenity::CreateMessage::new().embed(embed);
    let message = ctx.channel_id().send_message(ctx.http(), builder).await?;
    let list = ListThreads::from_message(message, ctx, header, target_category).await?;
    ctx.data().save_list(list)?;
    Ok(())
}

/// Get a link to all lists in the guild
#[poise::command(slash_command, prefix_command)]
async fn get_lists(ctx: Context<'_>) -> Result<(), Error> {
    let lists = ctx.data().lists()?;
    if lists.is_empty() {
        ctx.say("No lists found").await?;
        return Ok(());
    }
    // get a link to all list in that guild
    let mut msg_contents: Vec<String> = Vec::new();
    for list in lists
        .into_iter()
        .filter(|list| list.guild_id == ctx.guild_id().unwrap())
    {
        let message = ctx
            .http()
            .get_message(list.channel_id, list.message_id)
            .await?;
        msg_contents.push(message.link());
    }
    let msg = msg_contents.join("\n");
    ctx.say(msg).await?;
    Ok(())
}

/// Update all lists now, instead of waiting for the next scheduled update
#[poise::command(slash_command, prefix_command)]
async fn update(ctx: Context<'_>) -> Result<()> {
    update_lists(ctx.data(), ctx).await?;
    Ok(())
}

fn build_commands() -> Vec<poise::Command<Data, Error>> {
    vec![list(), unlist(), create_list(), get_lists()]
}

fn setup_db() -> Result<PickleDb> {
    let data_path_res = std::env::var("DATA_PATH");
    let data_path = if let Ok(data_path) = data_path_res.as_ref() {
        std::path::PathBuf::from(data_path)
    } else {
        let app_dir = directories::ProjectDirs::from("com", "llama_inc", "thread_bot")
            .expect("Failed to get project dirs");
        app_dir.data_dir().to_path_buf()
    };

    let db_path = data_path.join("threads.db");
    // mkdir parent if it doesn't exist
    let db = PickleDb::load(
        db_path.as_os_str(),
        PickleDbDumpPolicy::AutoDump,
        SerializationMethod::Json,
    );
    let db = if db.is_err() {
        let mut db = PickleDb::new(
            db_path.as_os_str(),
            PickleDbDumpPolicy::AutoDump,
            SerializationMethod::Json,
        );
        db.dump()?;
        db
    } else {
        db?
    };
    Ok(db)
}

fn build_framework(
    data: Data,
    commands: Vec<poise::Command<Data, Error>>,
) -> poise::Framework<Data, Error> {
    poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands,
            prefix_options: poise::PrefixFrameworkOptions {
                prefix: Some("!".into()),
                ..Default::default()
            },
            ..Default::default()
        })
        .setup(move |ctx, _ready, framework| {
            Box::pin(async move {
                poise::builtins::register_globally(ctx, &framework.options().commands).await?;
                update_lists(&data, ctx).await?;
                {
                    let cloned_ctx = ctx.clone();
                    let cloned_data = data.clone();
                    let every_10_minutes = every(10).minutes().at(20).perform(move || {
                        let _ctx = cloned_ctx.clone();
                        let _data = cloned_data.clone();
                        async move { update_lists(&_data, _ctx).await.unwrap() }
                    });
                    spawn(every_10_minutes);
                }

                Ok(data)
            })
        })
        .build()
}

#[tokio::main]
async fn main() -> Result<()> {
    let _ = dotenv();

    let token = std::env::var("DISCORD_TOKEN").expect("missing DISCORD_TOKEN");
    let db = setup_db()?;
    let data = Data::new(db)?;
    let framework = build_framework(data.clone(), build_commands());

    let intents =
        serenity::GatewayIntents::non_privileged() | serenity::GatewayIntents::MESSAGE_CONTENT;

    let mut client = serenity::ClientBuilder::new(token, intents)
        .framework(framework)
        .await?;

    client.start().await.unwrap();
    Ok(())
}
